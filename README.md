# xmlSimilarityTest

배민1.xml 파일 기준으로 유사도 계산 


# 실행

source venv/bin/activate

python3 xmlSimilarity.py



# 결과 : 

마트 xml 파일과 배민 xml 파일의 유사도는 (배민 xml 파일 기준) : 78.1% 입니다.

포장 xml 파일과 배민 xml 파일의 유사도는 (배민 xml 파일 기준) : 93.2% 입니다.
