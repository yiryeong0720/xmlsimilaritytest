import json
import xmltodict


def xmlSimilarity(xmlFile):
    dataDict_bemin = {}
    dataDict_xml = {}

    with open("./xmlFiles/bemin1.xml", 'rt', encoding="UTF-8") as f:
        json_data = xmltodict.parse(f.read())
        dataDict_bemin = json.loads(json.dumps(json_data))

    with open(xmlFile, 'rt', encoding="UTF-8") as f:
        json_data = xmltodict.parse(f.read())
        dataDict_xml = json.loads(json.dumps(json_data))

    stack = [[dataDict_bemin, dataDict_xml]]

    totalCount = 0
    diffCount = 0
    while stack:
        beminNextDict, xmlNextDict = stack.pop()
        bemin_key_list = list(beminNextDict.keys())

        if not bemin_key_list:
            break

        for key in bemin_key_list:
            if key == 'node':
                beminNextDict = beminNextDict[key]
                xmlNextDict = xmlNextDict[key]
                if isinstance(beminNextDict, list):
                    for idx in range(len(beminNextDict)-1, -1, -1):
                        stack.append([beminNextDict[idx], xmlNextDict[idx]])
                else:
                    if isinstance(xmlNextDict, list):
                        stack.append([beminNextDict, xmlNextDict[0]])
                    else:
                        stack.append([beminNextDict, xmlNextDict])
            else:
                totalCount += 1
                if beminNextDict[key] != xmlNextDict[key]:
                    diffCount += 1

    result = ''
    if xmlFile == "./xmlFiles/mart.xml":
        result += '마트 xml '
    else:
        result += '포장 xml '
    result += "파일과 배민 xml 파일의 유사도는 (배민 xml 파일 기준) : %.1f%%" % ((totalCount-diffCount)/totalCount*100) + " 입니다."
    print(result)


xmlSimilarity("./xmlFiles/mart.xml")
xmlSimilarity("./xmlFiles/pojang.xml")
